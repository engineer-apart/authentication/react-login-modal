export const defaultClasses = {
  'modal-container': 'eapax-modal-container',
  'modal-container-root': 'eapax-modal-container-root',
  'modal-content': 'eapax-modal-content',
  'modal-header': 'eapax-modal-header',
  'modal-close': 'eapax-modal-close',
  'modal-body': 'eapax-modal-body',
  'modal-additional-pre-content': 'eapax-modal-additional-pre-content',
  'modal-additional-post-content': 'eapax-modal-additional-post-content',
  'modal-footer': 'eapax-modal-footer',
  'social-anchor': 'eapax-social-anchor',
  'social-icon': 'eapax-social-icon',
  'social-text': 'eapax-social-text',
};

export const getRedirectUrl = (authUrl: string, strategy: string, clientId: string, redirect: string) => {
  return `${authUrl}?clientId=${clientId}&authStrategy=${strategy}&redirect=${encodeURIComponent(redirect)}`;
};
