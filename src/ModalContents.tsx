import React from 'react';

import { SocialButton } from './SocialButton';
import Facebook from './Facebook';
import Twitter from './Twitter';
import Google from './Google';
import { defaultClasses, getRedirectUrl } from './config';
import { Socials, ModalContentsProps } from './types';

const socials: Socials = {
  facebook: {
    icon: <Facebook />,
    text: 'facebook',
  },
  twitter: {
    icon: <Twitter />,
    text: 'twitter',
  },
  google: {
    icon: <Google />,
    text: 'google',
  },
};

const cx = (...classes: string[]) => {
  if (!classes.length) { return ''; }
  return classes.filter(Boolean).join(' ');
};

const ModalContents: React.FC<ModalContentsProps> = (props) => {
  const { authUrl, show, header, providers, clientId, redirectUrl, classes, onClose } = props;
  const classNames = { ...defaultClasses, ...classes };

  const renderSocialButton = (source: string) => {
    const social = socials[source];
    if (!social) { return null; }
    const text = `Continue with ${social.text}`;
    return (
      <SocialButton
        key={source}
        type={source}
        icon={social.icon}
        text={text}
        oauthUrl={getRedirectUrl(authUrl, source, clientId, redirectUrl)}
      />
    );
  };

  const capture = (e: React.MouseEvent) => {
    e.stopPropagation();
  }

  return (
    <div
      className={cx(classNames['modal-container'], classNames['modal-container-root'], show ? 'show' : '')}
      role="button"
      onClick={onClose}
      aria-label="Authentication modal background. Click to close the modal."
    >
      <div className={cx(classNames['modal-content'], show ? 'show' : '')} onClick={capture}>
        <div className={classNames['modal-header']}>
          {header}
          <button type="button" className={classNames['modal-close']} onClick={onClose}>&times;</button>
        </div>
        {props.precontent &&
          (<div className={classNames['modal-additional-pre-content']}>
            {props.precontent}
          </div>)}
        <div className={classNames['modal-body']}>
          {(providers || []).map(renderSocialButton)}
        </div>
        {props.postcontent &&
          (<div className={classNames['modal-additional-post-content']}>
            {props.postcontent}
          </div>)}
        {(providers || []).length === 0 && <p>I think you forgot to include some login providers.</p>}
        <div className={classNames['modal-footer']}></div>
      </div>
    </div>
  );
};

export default ModalContents;
