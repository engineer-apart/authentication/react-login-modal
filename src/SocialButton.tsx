import React from 'react';

import { defaultClasses } from './config';
import { SocialButtonProps } from './types';

export const SocialButton: React.FC<SocialButtonProps> = (props) => {
  const { type, icon, text, oauthUrl, classes } = props;
  const classNames = { ...defaultClasses, ...classes };

  return (
    <a className={classNames['social-anchor']} href={oauthUrl}>
      <div className={`${classNames['social-icon']} ${type}`}>
        {icon}
      </div>
      <div className={`${classNames['social-text']} ${type}`}>
        {text}
      </div>
    </a>
  );
};
