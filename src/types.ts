import React from 'react';

export type Classes = {
  [key: string]: string;
};

export type Social = {
  icon: React.ReactNode;
  text: string;
};

export type Socials = {
  [key: string]: Social;
};

export type SocialButtonProps = {
  type: string;
  icon: React.ReactNode;
  text: string;
  oauthUrl: string;
  classes?: Classes;
};

export type ModalContentsProps = {
  authUrl: string;
  show: boolean;
  header: React.ReactNode;
  precontent?: React.ReactNode;
  postcontent?: React.ReactNode;
  providers: string[];
  clientId: string;
  redirectUrl: string;
  classes?: Classes;
  ssr?: boolean;
  onClose?: () => void;
};

export type LoginModalProps = ModalContentsProps & {};
