import React from 'react';
import ReactDOM from 'react-dom';

import ModalContents from './ModalContents';
import { LoginModalProps } from './types';
import './styles.css';

const isBrowser = typeof window !== 'undefined';

export const LoginModal: React.FC<LoginModalProps> = (props) => isBrowser ?
  ReactDOM.createPortal(<ModalContents {...props} />, document.body) :
  props.ssr && <ModalContents {...props} /> || null;

